监控网址： 新浪股票接口http://www.cnblogs.com/kingwolfofsky/archive/2011/08/14/2138081.html

所用技术：js、JQuery、Notification浏览器消息推送

所需环境：windows、IIS或Linux，Apache；Mac也可以，只是一些html和js文件，要求不高，能搭建起网站就行；

Notification浏览器消息推送参考网址：https://blog.csdn.net/weixin_33985679/article/details/89559580，消息推送只能在https网站有效。

新浪股票接口参考文档：https://www.cnblogs.com/phpxuetang/p/4519446.html

如参考代码涉及侵权，请联系QQ:565539567，进行删除原贴。本帖分享此次开发经验，仅供大家参考！

需求：由于本人初次入股市，不懂行情，故想找一个实时监控股票价格，如波动较大则及时通知我。虽然很多手机App都有通知功能，但我只是想看看波动情况，每次都要看手机不太方便，所以想找一个web端的，因为工作的时候，PC上直接提示会方便的多。但苦于找了半天都没有找到合适的网站有提供，只能自己撸一个了。

背景：找到市面上提供了实时股票数据的网站，这是有很多的，但大多都不是https的由于[浏览器的同源安全策略](https://developer.mozilla.org/zh-CN/docs/Web/Security/Same-origin_policy)导致Notification消息推送不可用，那没有提示就没法愉快的玩耍了，如新浪的：http://finance.sina.com.cn/realstock/company/sz002271/nc.shtml?from=BaiduAladin，找了半天终于找到了一个https的网站：https://www.laohu8.com/stock/002271，刚高兴的把Notification消息推送的脚本加进去，坐等提示，结果半天没反应，然后发现这个网站居然不是实时动态更新数据的，坑啊。没办法，也没耐心接着找了，只是一个爬取数据，简单分析，给出提醒，功能也不难，索性就自己撸一个出来。

开始：前端

在head中引入<script type="text/javascript" src="http://hq.sinajs.cn/list=sz002271" charset="gb2312"></script>，这里用来获取股票的数据
![输入图片说明](https://images.gitee.com/uploads/images/2019/1227/121911_dea8cf12_5044691.png "屏幕截图.png")
浏览器消息推送代码实现，这段代码用到了浏览器通知，因为浏览器安全机制，只能在https或本地localhost中有效，如果发布后要将网站添加SSH证书，否则http中将不起作用。

var PERMISSON_GRANTED = 'granted';

        var PERMISSON_DENIED = 'denied';

        var PERMISSON_DEFAULT = 'default';



        Notification.requestPermission(function (res) {

            if (res === PERMISSON_GRANTED) {

                console.log('提醒开启成功！')

            }

        });

        //浏览器消息推送

        function createNotify(title, options) {

            if (Notification.permission === PERMISSON_GRANTED) {

                notify(title, options);

            } else {

                Notification.requestPermission(function (res) {

                    if (res === PERMISSON_GRANTED) {

                        notify(title, options);

                    }

                });

            }

            function notify($title, $options) {

                var notification = new Notification($title, $options);

            }

        }

动态加载js包，用来实时刷新获取股票信息

function loadJS(url, callback) {

            var script = document.createElement('script'),

                fn = callback || function () { };

            script.type = 'text/javascript';

            //IE

            if (script.readyState) {

                script.onreadystatechange = function () {

                    if (script.readyState == 'loaded' || script.readyState == 'complete') {

                        script.onreadystatechange = null;

                        fn();

                    }

                };

            } else {

                //其他浏览器

                script.onload = function () {

                    fn();

                };

            }

            script.src = url;

            document.getElementsByTagName('head')[0].children[3].remove();

            document.getElementsByTagName('head')[0].appendChild(script);

        }

设置监控参数，简单分析并定时刷新股票信息

var _time = 10,//定时秒数

            boval = 10,//波动值金额￥

            buy_num = 500,//买入股数

            buy_money = 7.72,//买入价格,0取开盘价,-1取昨收价

            _val = 100;//记录最近数据个数

        var intervalId,//定时器

            num = 0, //次数

            today_amount = 0,//今日盈亏

            maxVal = 0,

            minVal = 0;

        val = [];//值

        var zs_price = elements[2];//昨收

        var jk_price = elements[1];//今开

        var name = elements[0];

        $(document).ready(function () {

            intervalId = window.setInterval(function () {

                elements = hq_str_sz002271.split(",");

                var _code = 'hq_str_' + $('#code').val();

                var a = $(this)[0].frames['' + _code];

                if (a != undefined) {

                    elements = a.split(',');

                }

                if (document.title != elements[0]) {

                    maxVal = 0;

                    minVal = 0;

                }

                zs_price = elements[2];//昨收

                jk_price = elements[1];//今开

                document.title = elements[0];

                num++;

                var zx_price = elements[3];

                name = elements[0];

                buy_money = parseFloat($('#price').val()).toFixed(2);

                buy_num = parseInt($('#num').val());

                boval = parseFloat($('#boval').val()).toFixed(2);

                $('#kp_price').text(jk_price);

                var no = num % _val;

                var preno = (num - 1) % _val;

                var startval = buy_money;

                if (buy_money == 0) {

                    startval = jk_price;

                }

                else if (buy_money == -1) {

                    startval = zs_price;

                }

                var tarval = ((parseFloat(zx_price) - startval) * buy_num).toFixed(2);

                val[no] = tarval;

                if (parseFloat(tarval) > parseFloat(maxVal)) maxVal = parseFloat(tarval);

                if (parseFloat(tarval) < parseFloat(minVal) || minVal == 0) minVal = parseFloat(tarval);

                if (num > 1) {

                    if (Math.abs(val[no] - val[preno]) > boval) {

                        var d = new Date();

                        today_amount = ((parseFloat(zx_price) - zs_price) * buy_num).toFixed(2);

                        var msg1 = '股价：' + zx_price + '；当前盈亏：' + tarval.toString() + '；最高；' + maxVal.toString() + '；最低：' + minVal.toString() + '；波动：' + (val[no] - val[preno]).toString();

                        console.log('时间：' + d.toLocaleString() + ';' + name + '第' + no.toString() + '次，最近2次波动大于' + boval.toString() + '，请留意动向！上次:' + val[preno].toString() + ';本次：' + val[no].toString() + '；波动：' + (val[no] - val[preno]).toString());

                        createNotify(name + '预警通知', { body: msg1 });

                    }

                }

                today_amount = ((parseFloat(zx_price) - zs_price) * buy_num).toFixed(2);

                $('#real_price').text(zx_price);

                $('#real_amount').text(tarval);

                var msg = name + '当前股价：' + zx_price + '；当前盈亏：' + tarval.toString() + '；今日盈亏:' + today_amount.toString() + '；最高；' + maxVal.toString() + '；最低：' + minVal.toString();

                console.log(msg);

                //刷新接口

                loadJS('http://hq.sinajs.cn/list=' + $('#code').val(), function () {

                });

            }, 1000 * _time);

        })

到此核心业务代码就已经写完了，是不是很简单。前端页面做的很简单
![输入图片说明](https://images.gitee.com/uploads/images/2019/1227/121949_972d1b10_5044691.png "屏幕截图.png")
效果图
![输入图片说明](https://images.gitee.com/uploads/images/2019/1227/122008_f730d517_5044691.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1227/122022_5a34c151_5044691.png "屏幕截图.png")

现在就可以干自己的事了，如果股价有异常，会收到通知的。只要浏览器这个页面不关闭，页面可以最小化。

 ** 免责声明：股市有风险，投资需谨慎。本代码只提供参考，请根据自己情况仔细分析调整，如因本代码导致您操作异常，本作者不承担任何责任，请自行做好充分测试；** 

小结：1、接口为什么要使用script脚本引入的方式？

因为站点不同，涉及到js跨域操作，如果使用iframe方式，会导致js跨域获取失败，因为接口后端是没有把我们的请求网站添加为可信任的站点。我们不能改变提供方，就只能改变我们自己了，用脚步引入方式不受跨域限制；想详细了解js跨域问题的，可以参考[不要再问我js跨域的问题了](https://segmentfault.com/a/1190000015597029)

2、为什么要动态加载js包？

因为我们是通过js包来获取接口数据的，浏览器在解析js包时会获取接口的返回数据，但只在解析时获取，这样就只能获取一次接口数据，我们需要的是实时变化的股票数据，所以要在定时器中动态加载，这样才能保证是最新的数据。为避免加载的js重复过多，所以在加载前需要先移除之前的js包。

3、直接打开源码中的demo.html没有提示？

需要搭建本地网站，Notification浏览器消息推送只能在https和localhost中有效。如需远程访问，需在搭建站点时，添加SSH证书。